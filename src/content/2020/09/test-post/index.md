---
title: "Test Post!"
date: 2020-09-08T06:00:00.000+00:00
author: Gavin Vaught
featured_image: alex-hockett-h0kzXUklh-c-unsplash.jpg
caption: Souvlaki ignitus carborundum e pluribus unum. Epsum factorial non deposit quid pro quo hic escorol.
---

Test post with a [link](https://google.com)!

---

Here is some **emphasized text**. And here is some *italicized text*.

---

> A Blockquote
>
> It can span multiple lines, too!
> > You can even nest them.

---

Here is an unordered list:

- one item
- two items
- three items
  - etc.

---

Here is an ordered variation:

1. One
2. Two
3. Three
    1. One.One
4. you get the idea.

---

Single line of code: `echo "eat my shorts";`

---

Here is a code block:

    for ($i = 0; $i > 100; $i++) {
      if ($i % 8 === 0) {
        echo $i . PHP_EOL;
      }
    }