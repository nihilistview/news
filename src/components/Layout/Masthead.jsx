import React from 'react'
import styled from 'styled-components'

import Link from './../Link'

const Wrapper = styled.div`
  display: flex;
  align-content: center;
  justify-content: center;
  max-height: 11rem;
  .masthead {
    span {
      font-family: 'Broken Planewing', serif !important;
      font-size: 3rem;
    }
    font-weight: 640;
    text-decoration: none;
    font-variation-settings: 'slnt' -15, 'ital' 1;
    display: block;
    padding: 32px 0 28px;
    max-width: 500px;
  }
`

const Masthead = props => {
  return (
    <>
      <Wrapper>
        <Link className='masthead' to='/'>
          <span>
            The NihilistView News
          </span>
        </Link>
        </Wrapper>
    </>
  )
}

export default Masthead