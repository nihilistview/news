import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.nav`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  min-height: 3rem;
  height: 100%;
  border: 0 solid var(--gray-dark);
  border-top-width: 1px;
  border-bottom-width: 1px;
`

const Section = styled.section`
  display: flex;
  border: 0 solid var(--gray-dark);
  border-right-width: 1px;
  height: 2.8rem;
`

const Button = styled.button`
  display: grid;
  padding-left: 1.2rem;
  padding-right: 1.2rem;
  grid-template: 1fr / 1fr auto;
  align-items: center;
  background-color: transparent;
  border: none;
  font-size: .8rem;
  text-transform: uppercase;
  transition: opacity 256ms ease;
  cursor: pointer;
  > span {
    padding-left: .5rem;
  }
  :hover {
    opacity: .72;
  }
`

const Navigation = props => {
  return (
    <>
      <Wrapper>
        <Section>
          <Button>
            <svg stroke="currentColor" strokeWidth="2" width="16px" height="16px" viewBox="0 0 24 24"><circle fill="none" cx="9.417" cy="9.417" r="6.417"></circle><path fill="none" d="M21 21l-4.613-4.613"></path></svg>
          </Button>
        </Section>
        <Section>
          <Button className='nav-btn sections-btn'>
            <svg width="18px" height="22px" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2"><path d="M3 12h18M3 6h18M3 18h18"></path></svg>
            <span>Sections</span>
          </Button>
        </Section>
      </Wrapper>
    </>
  )
}

export default Navigation