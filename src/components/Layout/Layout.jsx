import React from 'react'

import Theme from './../../styles/theme'
import Masthead from './Masthead'
import Navigation from './Navigation'
import Container from './Container'
import useWindowSize from './../../hooks/useWindowSize'


export default function Layout(props, location) {
  const viewport = useWindowSize()
  let containerMaxWidth = '88vw'
  let gutterBool = true
  if (viewport.width > 1080) {
    containerMaxWidth = '64vw'
    gutterBool = false
  }
  return (
    <>
      <Theme />
      <Masthead />
      <Navigation />
      <Container 
        id='main-content' 
        maxWidth={containerMaxWidth} 
        disableGutters={gutterBool}
      >
        {props.children}
      </Container>
      {/* <Footer> */}
      {/* </Footer> */}
    </>
  )
}
