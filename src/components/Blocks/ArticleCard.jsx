import React from 'react'
import { Link } from 'gatsby'
import styled from 'styled-components'
import Img from 'gatsby-image'


const Article = styled.article`
  display: ${props => (props.image !== null) ? 'grid' : 'inline-grid'};
  ${(props) => props.image !== null && `
    grid-template: 1fr / 2fr 3fr;
    grid-gap: 4rem;
  `};
`

const Subheading = styled.p`
  font-size: 14px;
  line-height: 1.5;
  letter-spacing: 0;
  color: var(--gray);
`

const Byline = styled.div`
  font-size: 11px;
  line-height: 1.1;
  letter-spacing: .05em;
  text-transform: uppercase;
  color: var(--gray);
`

export default function ArticleCard(props) {
  const image = (props.image !== null) 
    ? (<Img 
        style={{ 'maxHeight': '420px' }}
        fluid={props.image.childImageSharp.fluid}
      />)
    : ''
  return (
    <Link to={props.slug}>
      <Article>
        <div>
          <h2>{props.title}</h2>
          <Subheading>{props.subheading}</Subheading>
          <Byline>
            <span>{props.author}</span> | <span>{props.publishedDatetime}</span>
          </Byline>
        </div>
        {image}
      </Article>
    </Link>
  )
}