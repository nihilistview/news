import React from 'react'
import { graphql } from 'gatsby'
import styled from 'styled-components'
import Img from 'gatsby-image'

import Layout from '../components/Layout/Layout'

const Header = styled.h1`
  font-family: var(--font-family-serif);
  font-size: 46px;
  line-height: 1.1;
  letter-spacing: -.016em;
`

const Figure = styled.figure`
  border-bottom: 1px solid var(--gray-dark);
  padding-bottom: 8px;
  margin-bottom: 32px;
  figcaption {
    font-size: 12px;
    line-height: 1.45;
    letter-spacing: 0;
    color: var(--gray);
  }
`

const Byline = styled.div`
  margin-bottom: 32px;
  display: flex;
  flex-direction: column;
  * {
    font-size: 12px;
    line-height: 1.45;
    letter-spacing: 0;
    color: var(--gray);
  }
`

export default function BlogPost({ data }) {
  const post = data.markdownRemark
  return (
    <Layout>
      <div>
        <Header>{post.frontmatter.title}</Header>
        <Figure>
          <Img 
            style={{ 'maxHeight': '420px' }}
            fluid={post.frontmatter.featured_image.childImageSharp.fluid} 
          />
          <figcaption>
            {post.frontmatter.caption}
          </figcaption>
        </Figure>
        <Byline>
          <div>By: {post.frontmatter.author}</div>
          <div>{post.frontmatter.date}</div>
        </Byline>
        <div dangerouslySetInnerHTML={{ __html: post.html }} />
      </div>
    </Layout>
  )
}

export const query = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt(pruneLength: 160)
      html
      frontmatter {
        title 
        author
        date(formatString: "H:mmA [on] MMMM DD, YYYY")
        featured_image {
          childImageSharp {
            fluid(maxWidth: 1200, quality: 92) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
        caption
      }
    }
  }
`
