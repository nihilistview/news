import React from "react"

import Layout from "../components/Layout/Layout"
import SEO from "../components/SEO"
import Link from '../components/Link'

const NotFoundPage = () => (
  <Layout>
    <SEO title="404: Not found" />
    <h1>NOT FOUND</h1>
    <p>You just hit a route that doesn&#39;t exist...</p>
    <p>Return <Link to='/'>home</Link>.</p>
  </Layout>
)

export default NotFoundPage
