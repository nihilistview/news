import React from 'react'
import { graphql } from 'gatsby'
// import styled from 'styled-components'

import Layout from '../components/Layout/Layout'
import ArticleCard from '../components/Blocks/ArticleCard'

// const ArticleGrid = styled.div`

// `

export default function Home({ data }) {
  return (
    <Layout>
      {data.allMarkdownRemark.edges.map(({ node }) => (
        <ArticleCard 
          key={node.id}
          slug={node.fields.slug}
          title={node.frontmatter.title}
          subheading={node.excerpt}
          author={node.frontmatter.author}
          publishedDatetime={node.frontmatter.date}
          image={node.frontmatter.featured_image}
        />
      ))}
    </Layout>
  )
}

export const query = graphql`
  query {
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            author
            date(formatString: "ddd hA")
            featured_image {
              childImageSharp {
                fluid(maxWidth: 420, quality: 92) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
          }
          fields {
            slug
          }
          excerpt
        }
      }
    }
  }
`
