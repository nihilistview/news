module.exports = {
  siteMetadata: {
    title: `Nihilistview News`,
    titleTemplate: ``,
    description: ``,
    siteUrl: `https://news.nihilistview.com`,
    image: `/static/assets/favicon.png`,
    author: `@`,
  },
  pathPrefix: `/nihilistview/news` /* For gitlab-ci */,
  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `src`,
        path: `${__dirname}/src/`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `assets`,
        path: `${__dirname}/static/assets`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `fonts`,
        path: `${__dirname}/src/fonts`,
      },
    },
    `gatsby-plugin-netlify-cms`,
    'gatsby-transformer-remark',
    `gatsby-transformer-sharp`, 
    `gatsby-plugin-sharp`,
    // {
    //   resolve: `gatsby-plugin-google-analytics`,
    //   options: {
    //     trackingId: 'UA-139960866-1',
    //     head: 'true',
    //     anonymize: true,
    //     respectDNT: true,
    //     exclude: ['/contact/success/**', '/account'],
    //     pageTransitionDelay: 0,
    //     sampleRate: 5,
    //     siteSpeedSampleRate: 10,
    //     cookieDomain: 'gavinvaught.com',
    //     forceSSL: true,
    //   },
    // },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Nihilistview News`,
        short_name: `NVN`,
        start_url: `/`,
        icon: 'static/assets/favicon.png',
        background_color: `#000`,
        theme_color: `#121212`,
        display: `minimal-ui`,
      },
    },
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-offline`,
    {
      resolve: `gatsby-plugin-styled-components`,
      options: {
        displayName: false,
      },
    },
  ],
}
