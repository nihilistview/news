# Nihilistview News

Gatsby application with a Netlify CMS backend. When content is created/updated, a commit/merge is made to the GitLab repo, a CI/CD pipeline is triggered, once finished, the application is then deployed on Netlify via a deploy hook.

Live project can be viewed [here](https://nihilistview.netlify.app/).